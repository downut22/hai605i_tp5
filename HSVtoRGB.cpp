// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;

void HSVtoRGB(char* cNomImgOut, char* cNomImgH, char* cNomImgS, char* cNomImgV)
{
  int nH, nW, nTaille;
  OCTET *ImgOut,*ImgH,*ImgS,*ImgV;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgH, &nH, &nW);
   nTaille = nH * nW;
  
  int t3 = nTaille * 3;
   allocation_tableau(ImgOut, OCTET, t3);
   allocation_tableau(ImgH, OCTET, nTaille);
  allocation_tableau(ImgS, OCTET, nTaille);
  allocation_tableau(ImgV, OCTET, nTaille);
   lire_image_pgm(cNomImgH, ImgH, nTaille);
lire_image_pgm(cNomImgS, ImgS, nTaille);
  lire_image_pgm(cNomImgV, ImgV, nTaille);
  
  for(int i = 0; i < t3;i+=3)
  {    
      float H = ImgH[i/3]/255.0;
      float S = ImgS[i/3]/255.0;
      float V = ImgV[i/3]/255.0;
      float r,g,b;
    
      if(S <= 0){r=V;g=V;b=V;continue;}

      H /= 60.0;
      float hh = (float)(H);
      float ff = H-hh;
      float p = V * (1-S);
      float q = V * (1-(S*ff));
      float t = V * (1-(S*(1-ff)));

    switch((int)(hh)) {
      case 0:
        r = V;
        g = t;
        b = p;
        break;
      case 1:
        r = q;
        g = V;
        b = p;
        break;
      case 2:
        r = p;
        g = V;
        b = t;
        break;

      case 3:
        r = p;
        g = q;
        b = V;
        break;
      case 4:
        r = t;
        g = p;
        b = V;
        break;
      case 5:
      default:
        r = V;
        g = p;
        b = q;
        break;
    }

    ImgOut[i] = r*255;
    ImgOut[i+1] = g*255;
    ImgOut[i+2] = b*255;
  }

   ecrire_image_ppm(cNomImgOut, ImgOut,  nH, nW);
   free(ImgOut); free(ImgH); free(ImgS); free(ImgV);
}

int main(int argc, char* argv[])
{
  char cNomImgOut[250], cNomImgH[250], cNomImgS[250], cNomImgV[250];

  if (argc != 5) 
     {
       printf("Usage: ImageOut.ppm  ImgH.pgm ImgS.pgm ImgV.pgm\n"); 
       exit (1) ;
     }
  printf("Lecture des args\n");

   sscanf (argv[1],"%s",cNomImgOut) ;
   sscanf (argv[2],"%s",cNomImgH);
  sscanf (argv[3],"%s",cNomImgS);
  sscanf (argv[4],"%s",cNomImgV);
  
  HSVtoRGB(cNomImgOut,cNomImgH,cNomImgS,cNomImgV);
  
   return 1;
}
