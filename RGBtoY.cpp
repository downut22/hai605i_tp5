// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;

void RGBtoY(char* cNomImgLue, char* cNomImgEcrite)
{
  int nH, nW, nTaille;
  OCTET *ImgIn,*ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
  int t3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, t3);
   allocation_tableau(ImgOut, OCTET, nTaille);
   lire_image_ppm(cNomImgLue, ImgIn, nTaille);

  for(int i = 0; i < t3;i+=3)
  {    
      ImgOut[i/3] += (ImgIn[i]*0.299);
      ImgOut[i/3] += (ImgIn[i+1]*0.587);
      ImgOut[i/3] += (ImgIn[i+2]*0.114);
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];

  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm  ImgOut.ppm count\n"); 
       exit (1) ;
     }
  printf("Lecture des args\n");

   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

    RGBtoY(cNomImgLue,cNomImgEcrite);
  
   return 1;
}
