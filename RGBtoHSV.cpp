// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;

void RGBtoHSV(char* cNomImgLue, char* cNomImgH, char* cNomImgS, char* cNomImgV)
{
  int nH, nW, nTaille;
  OCTET *ImgIn,*ImgH,*ImgS,*ImgV;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
  int t3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, t3);
   allocation_tableau(ImgH, OCTET, nTaille);
  allocation_tableau(ImgS, OCTET, nTaille);
  allocation_tableau(ImgV, OCTET, nTaille);
   lire_image_ppm(cNomImgLue, ImgIn, nTaille);

  for(int i = 0; i < t3;i+=3)
  {    
      float r = ImgIn[i]/(float)255;
      float g = ImgIn[i+1]/(float)255;
      float b = ImgIn[i+2]/(float)255;

      float cmax = max(r,max(g,b));
      float cmin = min(r,min(g,b));
      float diff = cmax-cmin;

      float H = 
        (cmax==0) ? 0 :
        (cmax==r) ? (int)((60*((g-b)/diff)+360))%360 :
        (cmax==g) ? (int)((60*((b-r)/diff)+120))%360 :
        (cmax==b) ? (int)((60*((r-g)/diff)+240))%360 :0;

      int S = 
        (cmax==0) ? 0 : (diff/cmax)*100;

      int V = cmax*100;
      
      ImgH[i/3] = (H/(float)360)*255;

      ImgS[i/3] = S;

      ImgV[i/3] = V;
    
  }

   ecrire_image_pgm(cNomImgH, ImgH,  nH, nW);
  ecrire_image_pgm(cNomImgS, ImgS,  nH, nW);
  ecrire_image_pgm(cNomImgV, ImgV,  nH, nW);
   free(ImgIn); free(ImgH); free(ImgS); free(ImgV);
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgH[250], cNomImgS[250], cNomImgV[250];

  if (argc != 5) 
     {
       printf("Usage: ImageIn.ppm  ImgH.ppm ImgS.ppm ImgV.ppm\n"); 
       exit (1) ;
     }
  printf("Lecture des args\n");

   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgH);
  sscanf (argv[3],"%s",cNomImgS);
  sscanf (argv[4],"%s",cNomImgV);
  
  RGBtoHSV(cNomImgLue,cNomImgH,cNomImgS,cNomImgV);
  
   return 1;
}
