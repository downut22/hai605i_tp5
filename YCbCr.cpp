// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;

void RGBtoYCrCb(char* cNomImgOut, char* cNomImgY, char* cNomImgCb, char* cNomImgCr)
{
  int nH, nW, nTaille;
  OCTET *ImgOut,*ImgY,*ImgCr,*ImgCb;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgY, &nH, &nW);
   nTaille = nH * nW;
  
  int t3 = nTaille * 3;
   allocation_tableau(ImgOut, OCTET, t3);
   allocation_tableau(ImgY, OCTET, nTaille);
  allocation_tableau(ImgCr, OCTET, nTaille);
  allocation_tableau(ImgCb, OCTET, nTaille);
   lire_image_pgm(cNomImgY, ImgY, nTaille);
lire_image_pgm(cNomImgCr, ImgCr, nTaille);
  lire_image_pgm(cNomImgCb, ImgCb, nTaille);
  
  for(int i = 0; i < t3;i+=3)
  {    
      int v =  ImgY[i/3]; 
      ImgOut[i] = min(max(0,v),255);
        
      v = ImgCb[i/3];  
      ImgOut[i+1] = min(max(0,v),255);
            
      v = ImgCr[i/3];  
      ImgOut[i+2] = min(max(0,v),255);
    }

   ecrire_image_ppm(cNomImgOut, ImgOut,  nH, nW);
   free(ImgOut); free(ImgY); free(ImgCb); free(ImgCb);
}

int main(int argc, char* argv[])
{
  char cNomImgOut[250], cNomImgY[250], cNomImgCr[250], cNomImgCb[250];

  if (argc != 5) 
     {
       printf("Usage: ImageOut.ppm  ImgY.pgm ImgCr.pgm ImgCb.pgm\n"); 
       exit (1) ;
     }
  printf("Lecture des args\n");

   sscanf (argv[1],"%s",cNomImgOut) ;
   sscanf (argv[2],"%s",cNomImgY);
  sscanf (argv[3],"%s",cNomImgCr);
  sscanf (argv[4],"%s",cNomImgCb);
  
  RGBtoYCrCb(cNomImgOut,cNomImgY,cNomImgCb,cNomImgCr);
  
   return 1;
}
